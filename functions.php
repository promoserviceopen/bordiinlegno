<?php

function get_site_parameter($param_name)
{
	$contents = file_get_contents('site_owner_data.json');
    $jsonArray = json_decode($contents,true);

    return $jsonArray[$param_name];
}

add_action('woocommerce_before_main_content', 'remove_sidebar' );
function remove_sidebar()
{
	if( is_checkout() || is_cart() || is_product()) { 
	 remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10);
   }
}

remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10);
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );
